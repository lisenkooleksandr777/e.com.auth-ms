package auth

import (
	"errors"
	"net/http"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"

	authModels "e/auth/models/auth"

	authService "e/auth/services/auth"
	"e/auth/utils/config"
)

//Auth post endpoint that will create or update USER
func Auth(c echo.Context) error {
	var data authModels.User
	err := c.Bind(&data)

	//TODO: error handling should be smarter including different codes etc.
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	result, err := authService.Auth(&data)

	//TODO: make errors more flexible
	if err != nil {
		return c.JSON(http.StatusInternalServerError, errors.New("=( Oops, Somehing went wrong"))
	}

	// Set custom claims
	claims := &authModels.JwtUserClaims{
		result.IDentifier,
		result.Name,
		result.PictureURL,
		result.Permissions,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
		},
	}
	// Create token with claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte(config.GetSecret()))

	if err != nil {
		return c.JSON(http.StatusInternalServerError, errors.New("=( Oops, Somehing went wrong"))
	}

	return c.JSON(http.StatusOK, authModels.NewAuthResult(t, claims))
}

//ByToken verify existing users
func ByToken(c echo.Context) error {

	user := c.Get("user").(*jwt.Token)

	if user == nil {
		return c.JSON(http.StatusForbidden, errors.New("Authentication data is not correct"))
	}

	claims := user.Claims.(*authModels.JwtUserClaims)

	if claims == nil {
		return c.JSON(http.StatusForbidden, errors.New("Authentication data is not correct"))
	}

	return c.JSON(http.StatusOK, authModels.NewAuthResult("", claims))
}

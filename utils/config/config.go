package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"time"
)

//Configuration represent server configuration
type Configuration struct {
	DataBaseURL           string   `json:"dataBaseUrl"`
	MongoTimeOut          int64    `json:"mongoTimeOut"`
	LoggerPath            string   `json:"loggerPath"`
	ServerPort            int      `json:"serverPort"`
	ConnectionString      string   `json:"connectionString"`
	CacheConnectionString string   `json:"cacheConnectionString"`
	AllowOrigins          []string `json:"allowOrigins"`
	RabbintMQHost         string   `json:"rabbitMQHost"`
	RabbintMQUser         string   `json:"rabbitMQuser"`
	RabbintMQPass         string   `json:"rabbitMQPass"`
	Neo4jHost             string   `json:"neo4jHost"`
	Neo4jUser             string   `json:"neo4jUser"`
	Neo4jPass             string   `json:"neo4jPass"`
	Neo4jTimeOut          int64    `json:"neo4jTimeOut"`
	Secret                string   `json:"secret"`
	MysqlConnectionString string   `json:"mysqlConnectionString"`
	MysqlTimeOut          int64    `json:"mysqlTimeOut"`
}

//config in
var config Configuration

func init() {
	dat, err := ioutil.ReadFile("config.json")
	check(err)
	err = json.Unmarshal(dat, &config)
	check(err)
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

//GetMysqlTimeOut returns timeout for sql timeout
func GetMysqlTimeOut() int64 {
	return config.MysqlTimeOut
}

//GetMysqlConnectionString returns connection string in format GetMysqlTimeOut
func GetMysqlConnectionString() string {
	return config.MysqlConnectionString
}

//GetSecret for genration of signature
func GetSecret() string {
	return config.Secret
}

//GetAllowedOrigins get urls for CORS
func GetAllowedOrigins() []string {
	return config.AllowOrigins
}

//GetMongoConnectionTimeOut get timeout for mongo connection
func GetMongoConnectionTimeOut() time.Duration {
	return time.Duration(config.MongoTimeOut * int64(time.Second))
}

//GetNeo4JConnectionTimeOut get timeout for neo4j connection
func GetNeo4JConnectionTimeOut() time.Duration {
	return time.Duration(config.Neo4jTimeOut * int64(time.Second))
}

//GetRabbitMQConnectionString return connection string for rabbitmq host
func GetRabbitMQConnectionString() string {
	return fmt.Sprintf("amqp://%s:%s@%s", config.RabbintMQUser, config.RabbintMQPass, config.RabbintMQHost)
}

// GetNeo4JUser returns decoded user name
func GetNeo4JUser() string {
	return config.Neo4jUser
}

// GetNeo4JHost returns the bolt host
func GetNeo4JHost() string {
	return config.Neo4jHost
}

// GetNeo4JPassword return decoded neo4j password
func GetNeo4JPassword() string {
	return config.Neo4jPass
}

//GetDataBaseURL get url of database
func GetDataBaseURL() string {
	return config.DataBaseURL
}

//GetConnectionString get mondo conection string
func GetConnectionString() string {
	return config.ConnectionString
}

//GetServerPort get port that we use to host the app
func GetServerPort() int {
	return config.ServerPort
}

//GetCacheConnectionSrtring return connectino url to cache instance
func GetCacheConnectionSrtring() string {
	return config.CacheConnectionString
}

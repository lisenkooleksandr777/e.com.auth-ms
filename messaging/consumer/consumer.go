package consumer

import (
	"encoding/json"
	"errors"

	"github.com/streadway/amqp"

	"e/auth/repositories/auth/graphsynch"
	"e/auth/repositories/auth/mysqlsync"
	"e/auth/utils/config"

	"e/auth/models/auth"
)

const (
	//usersToSynchronize topic of created events which should be synchronized
	usersToSynchronize = "users_to_synchronize"
)

//Init initialization of consumer package
func Init() {
	go initUsersToSynchronizeConsumer()
}

func initUsersToSynchronizeConsumer() {
	connection, err := amqp.Dial(config.GetRabbitMQConnectionString())

	if err != nil {
		panic(errors.New("could not establish connection with RabbitMQ:" + err.Error()))
	}

	channel, err := connection.Channel()

	if err != nil {
		panic(errors.New("could not open RabbitMQ channel:" + err.Error()))
	}

	// We consume data from the queue named Test using the channel we created in go.
	msgs, err := channel.Consume(usersToSynchronize, "", false, false, false, false, nil)

	if err != nil {
		panic(errors.New("error consuming the queue: " + err.Error()))
	}

	// We loop through the messages in the queue and print them in the console.
	// The msgs will be a go channel, not an amqp channel
	for msg := range msgs {

		var user auth.User
		err := json.Unmarshal(msg.Body, &user)

		if err == nil {
			err := graphsynch.MergeUser(&user)
			if err == nil {
				err := mysqlsync.MergeUser(&user)
				if err == nil {
					msg.Ack(false)
				}
				//msg.Ack(false)
			}
		}
	}

	// We close the connection after the operation has completed.
	//defer connection.Close()
}

package producer

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/streadway/amqp"

	"e/auth/utils/config"

	"e/auth/models/auth"
)

const (

	//usersToSynchronize topic of created events which should be synchronized
	usersToSynchronize = "users_to_synchronize"
)

var channel *amqp.Channel

//Init initialization of producer package
func Init() {
	connection, err := amqp.Dial(config.GetRabbitMQConnectionString())

	if err != nil {
		panic(errors.New("Could not establish connection with RabbitMQ:" + err.Error()))
	}

	// Create a channel from the connection. We'll use channels to access the data in the queue rather than the
	// connection itself
	ch, err := connection.Channel()

	if err != nil {
		panic(errors.New("Could not open RabbitMQ channel:" + err.Error()))
	}

	q, err := ch.QueueDeclare(
		usersToSynchronize,
		true,
		false,
		false,
		false,
		nil)

	if err != nil {
		panic(errors.New("Could not declar RabbitMQ que for deleted events:" + err.Error()))
	} else {
		fmt.Println(fmt.Sprintf("Changel %s was declared.", q.Name))
	}

	channel = ch
}

//PublishUserToSyncQue - publishing file to delete
func PublishUserToSyncQue(user *auth.User) error {
	// We create a message to be sent to the queue.
	// It has to be an instance of the aqmp publishing struct
	body, err := json.Marshal(user)

	if err != nil {
		return err
	}

	message := amqp.Publishing{
		Body: body,
	}

	return channel.Publish("", usersToSynchronize, false, false, message)
}

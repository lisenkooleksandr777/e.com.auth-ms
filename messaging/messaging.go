package messaging

import (
	"e/auth/messaging/consumer"
	"e/auth/messaging/producer"
)

//to run rabbitmq container
//docker run --detach --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management

//Init of comunication to rabbit mq messaging
func Init() {
	producer.Init()

	consumer.Init()
}

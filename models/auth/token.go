package auth

import (
	"encoding/json"
	"time"
)

//Token represent user credentials
type Token struct {
	UserID                   string    `bson:"userID"  json:"userId"`
	TokenString              string    `bson:"tokenString"  json:"tokenString"`
	TokenID                  string    `bson:"tokenId"  json:"tokenId"`
	DataAccessExpirationDate time.Time `bson:"dataAccessExpirationDate" json:"dataAccessExpirationDate"`
	ExpirationDate           time.Time `bson:"expirationDate" json:"expirationDate"`
	RefreshDate              time.Time `bson:"refreshDate" json:"refreshDate"`
}

// UnmarshalBinary Implement interface for custom unmarshalling of data for reddis caching
func (token *Token) UnmarshalBinary(data []byte) error {
	return json.Unmarshal(data, token)
}

// MarshalBinary Implement interface for custom marshalling of data for reddis caching
func (token *Token) MarshalBinary() (data []byte, err error) {
	return json.Marshal(token)
}

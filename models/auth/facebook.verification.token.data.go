package auth

type FaceBookTokenVerificationData struct {
	Error *FaceBookTokenError `json:"error"`
	Id    string              `json:"id"`
	Name  string              `json:"name"`
}

type FaceBookTokenError struct {
	Message         string `json:"message"`
	Code            int    `json:"code"`
	Type            string `json:"type"`
	FaceBookTraceId string `json:"fbtrace_id"`
}

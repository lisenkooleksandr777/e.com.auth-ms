package auth

import (
	"encoding/json"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"e/auth/models/entities"
)

// User represent user data in database/users collection
type User struct {
	IDentifier primitive.ObjectID `bson:"_id"  json:"id"`

	Provider    string   `bson:"provider"  json:"provider"`
	Name        string   `bson:"name"  json:"name"`
	Gender      string   `bson:"gender"  json:"gender"`
	Birthday    string   `bson:"birthday"  json:"birthday"`
	Email       string   `bson:"email"  json:"email"`
	PictureURL  string   `bson:"pictureURL"  json:"pictureUrl"`
	Permissions []string `bson:"permissions"  json:"permissions"`

	GoogleToken   *Token            `bson:"googleToken" json:"googleToken"`
	FaceBookToken *Token            `bson:"faceBookToken" json:"faceBookToken"`
	Setting       *entities.Setting `bson:"setting"  json:"setting"`
}

// UnmarshalBinary Implement interface for custom unmarshalling of data for reddis caching
func (userData *User) UnmarshalBinary(data []byte) error {
	return json.Unmarshal(data, userData)
}

// MarshalBinary Implement interface for custom marshalling of data for reddis caching
func (userData *User) MarshalBinary() (data []byte, err error) {
	return json.Marshal(userData)
}

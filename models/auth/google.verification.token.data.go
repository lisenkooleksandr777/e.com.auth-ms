package auth

type GoogleTokenVerificationData struct {
	Error string `json:"error"`
	ID    string `json:"sub"`
	Name  string `json:"name"`
	Email string `json:"email"`
}

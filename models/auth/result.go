package auth

//Result contains data of user + token
type Result struct {
	Token    string         `json:"token"`
	UserData *JwtUserClaims `json:"userData"`
}

//NewAuthResult create new Result
func NewAuthResult(token string, userData *JwtUserClaims) *Result {
	var a Result
	a.Token = token
	a.UserData = userData
	return &a
}

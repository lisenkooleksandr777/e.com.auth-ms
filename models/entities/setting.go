package entities

// Setting user setting
type Setting struct {
	Range       int32 `bson:"range" json:"range"`
	DaysForward int32 `bson:"daysForward" json:"daysForward"`
}

// GetDefaultSetting return default setting for user if it for the first time here
func GetDefaultSetting() *Setting {
	var result = new(Setting)
	result.Range = 30
	result.DaysForward = 7
	return result
}

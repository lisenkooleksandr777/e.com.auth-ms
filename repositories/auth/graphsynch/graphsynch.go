package graphsynch

import (
	"fmt"

	"e/auth/utils/config"

	bolt "github.com/johnnadratowski/golang-neo4j-bolt-driver"

	"e/auth/models/auth"
)

const (
	mergeUser = "MERGE (u:USER {mongoId: $userMongoId })  ON CREATE SET u += { name: $name, avatarURL:  $avatarURL }  ON MATCH SET u += { name: $name, avatarURL:  $avatarURL }"
)

// MergeUser will add just created user to the neo4j claster
func MergeUser(user *auth.User) error {
	var con, err = createConnection()

	if err != nil {
		return err
	}

	defer con.Close()

	_, err = con.ExecNeo(mergeUser, map[string]interface{}{
		"userMongoId": user.IDentifier.Hex(),
		"name":        user.Name,
		"avatarURL":   user.PictureURL,
	})

	return err
}

func createConnection() (bolt.Conn, error) {
	driver := bolt.NewDriver()
	con, err := driver.OpenNeo(getConnectionString())
	return con, err
}

func getConnectionString() string {
	return fmt.Sprintf("bolt://%s:%s@%s", config.GetNeo4JUser(), config.GetNeo4JPassword(), config.GetNeo4JHost())
}

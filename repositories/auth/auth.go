package auth

import (
	"context"
	"errors"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	bson_ "go.mongodb.org/mongo-driver/bson"

	authModels "e/auth/models/auth"
	"e/auth/utils/config"
)

const (
	usersCollection = "users"
)

//CreateOrUpdateUser if facebook token expired or is first time authentication we will update
// or create new token
func CreateOrUpdateUser(user *authModels.User) (*authModels.User, error) {

	userFilter := createUserFilterByToken(user)
	userUpdatedEntity := createUpsertUserEntity(user)

	mongoClient, err := createConnection()

	if err != nil {
		return nil, err
	}

	defer mongoClient.Disconnect(context.TODO())

	upsert := true

	var collection = mongoClient.Database("e_com").Collection(usersCollection)

	udatedResult, err := collection.UpdateOne(context.Background(), userFilter, userUpdatedEntity, &options.UpdateOptions{Upsert: &upsert})

	if err != nil {
		return nil, errors.New("Internal server error")
	}
	var upsertedUser *authModels.User

	if udatedResult.UpsertedCount == 1 {
		res := collection.FindOne(context.Background(), bson_.D{{"_id", udatedResult.UpsertedID}}, options.FindOne())
		err = res.Decode(&upsertedUser)
	} else {
		res := collection.FindOne(context.Background(), userFilter, options.FindOne())
		err = res.Decode(&upsertedUser)
	}

	if err != nil {
		return nil, err
	}
	return upsertedUser, nil
}

func createUserFilterByToken(user *authModels.User) *bson_.D {
	var result *bson_.D

	switch user.Provider {
	case "FACEBOOK":
		result = &bson_.D{
			{"faceBookToken.userID", user.FaceBookToken.UserID},
		}
	case "GOOGLE":
		result = &bson_.D{
			{"googleToken.userId", user.GoogleToken.UserID},
		}
	case "IG":
		result = &bson_.D{
			{"igToken", ""},
		}
	}
	return result
}

//TODO: Add setting updation
func createUpsertUserEntity(user *authModels.User) *bson_.D {
	var result *bson_.D
	switch user.Provider {
	case "FACEBOOK":
		result = &bson_.D{
			{"$set", bson_.D{
				{"name", user.Name},
				{"gender", user.Gender},
				{"birthday", user.Birthday},
				{"email", user.Email},
				{"pictureURL", user.PictureURL},
				{"faceBookToken", bson_.D{
					{"userID", user.FaceBookToken.UserID},
					{"tokenString", user.FaceBookToken.TokenString},
					{"dataAccessExpirationDate", user.FaceBookToken.DataAccessExpirationDate},
					{"expirationDate", user.FaceBookToken.ExpirationDate},
					{"refreshDate", user.FaceBookToken.RefreshDate},
				}},
			},
			}}
	case "GOOGLE":
		result = &bson_.D{
			{"$set", bson_.D{
				{"name", user.Name},
				{"gender", user.Gender},
				{"birthday", user.Birthday},
				{"email", user.Email},
				{"pictureURL", user.PictureURL},
				{"googleToken", bson_.D{
					{"userId", user.GoogleToken.UserID},
					{"tokenString", user.GoogleToken.TokenString},
					{"tokenId", user.GoogleToken.TokenID},
					{"dataAccessExpirationDate", user.GoogleToken.DataAccessExpirationDate},
					{"expirationDate", user.GoogleToken.ExpirationDate},
					{"refreshDate", user.GoogleToken.RefreshDate},
				}},
			},
			}}
	}
	return result
}

//...........Create Connection to mongo ............

func createConnection() (*mongo.Client, error) {

	client, err := mongo.Connect(context.TODO(),
		options.Client().ApplyURI(config.GetConnectionString()).SetConnectTimeout(config.GetMongoConnectionTimeOut()))

	if err != nil {
		log.Fatal(err)
	}

	return client, err
}

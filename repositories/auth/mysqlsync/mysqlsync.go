package mysqlsync

import (
    "database/sql"

	_ "github.com/go-sql-driver/mysql"
	
	"e/auth/utils/config"
	"e/auth/models/auth"
)

const (
	mergeuserQuery = "replace into users (mongo_id, user_name, avatar_url) values (?, ?, ?)"
)
// MergeUser will add just created user to the neo4j claster
func MergeUser(user *auth.User) error {

	db, err := sql.Open("mysql", config.GetMysqlConnectionString())

	defer db.Close()

	if err!= nil {
		return err
	}

	_, err = db.Exec(mergeuserQuery, user.IDentifier.Hex(), user.Name, user.PictureURL)

	if err != nil{
		return err
	}

	return nil
}
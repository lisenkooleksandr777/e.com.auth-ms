package auth

import (
	"encoding/json"
	"errors"
	"io/ioutil"

	"net/http"

	authModels "e/auth/models/auth"
	authRepository "e/auth/repositories/auth"

	"e/auth/messaging/producer"
)

const (
	appName = "e.com"
	appID   = "540336066331193"
)

//Auth authenticate users that have not loggined before
func Auth(user *authModels.User) (*authModels.User, error) {

	var token = ""
	if user.Provider == "FACEBOOK" {

		token = user.FaceBookToken.TokenString

		err := verifyFacebookToken(token)

		if err != nil {
			return nil, err
		}
	} else if user.Provider == "GOOGLE" {
		err := verifyGoogleToken(user)

		if err != nil {
			return nil, err
		}
	}

	userData, err := authRepository.CreateOrUpdateUser(user)

	if err == nil {
		err = producer.PublishUserToSyncQue(userData)
		if err != nil {
			//TODO make some retry logic here
			err = nil
		}
	}

	return userData, err

}

func verifyFacebookToken(token string) error {
	//should be called this method with given acces token
	//https://graph.facebook.com/540336066331193/?access_token=EAAHrbstLAjkBALszgq5bEoiJw01Gw9mIZB0d9pZAXTXjEu9jRiUtjNZC7KlHKodWZA28UE7lT2DtrBwSQjnNKDqBobLZCEPXZAfEHKWWv0M6TFlYJfv0pZCa42KVpkzmMJeSlZBvPpghrY9iNsFK3M7Vtii2J7u1HaTxFVxI1i6EsJkbJNCyoq7KXvyYbFqpCiKOmW0mnREHOAZDZD
	var traceLink = "https://graph.facebook.com/" + appID + "/?access_token=" + token
	resp, err := http.Get(traceLink)

	if err != nil {
		return err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return err
	}
	var data authModels.FaceBookTokenVerificationData
	err = json.Unmarshal(body, &data)
	if err != nil {
		return err
	}
	if data.Error != nil {
		return errors.New("Authentication data is not correct")
	} else if data.Id == appID {
		return nil
	}

	return nil
}

//verification of google user
//https://developers.google.com/identity/protocols/oauth2/openid-connect#validatinganidtoken
//oauth2.googleapis.com/tokeninfo?id_token
func verifyGoogleToken(user *authModels.User) error {

	var traceLink = "https://oauth2.googleapis.com/tokeninfo?id_token=" + user.GoogleToken.TokenID
	resp, err := http.Get(traceLink)

	if err != nil {
		return err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return err
	}
	var data authModels.GoogleTokenVerificationData
	err = json.Unmarshal(body, &data)
	if err != nil {
		return err
	}
	if data.Error != "" {
		return errors.New("Authentication data is not correct")
	} else if data.ID != user.GoogleToken.UserID || data.Email != user.Email || data.Name != user.Name {
		return errors.New("User data is not matched")
	}

	return nil
}

package main

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	"e/auth/utils/config"

	authModels "e/auth/models/auth"

	"e/auth/messaging"

	authEnpoints "e/auth/endpoints/auth"
)

func main() {
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		Skipper:      middleware.DefaultSkipper,
		AllowOrigins: config.GetAllowedOrigins(),
		AllowMethods: []string{http.MethodPost, http.MethodGet},
	}))

	e.POST("/", authEnpoints.Auth)

	r := e.Group("/authbytoken")
	// Configure middleware with the custom claims type
	//this part should be shared accross microservices to be used as uthentication approach
	jwtConfig := middleware.JWTConfig{
		Claims:     &authModels.JwtUserClaims{},
		SigningKey: []byte(config.GetSecret()),
	}
	r.Use(middleware.JWTWithConfig(jwtConfig))
	r.GET("/", authEnpoints.ByToken)

	messaging.Init()

	// Start server
	e.Logger.Fatal(e.Start(fmt.Sprintf(":%d", config.GetServerPort())))
}
